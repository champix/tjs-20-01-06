 
const ADR_SRV='http://localhost:666';
/**
 * 
 * @param {Object} params 
 *
 */
export default function httpGet(params,callback) {
      //ressource
      //id * 
      //value by def. if not present
      if(undefined===params.ressource)params.ressource='users';
    let url=ADR_SRV+'/'+params.ressource;
    //complete with id if present
    if(undefined!==params.id)url+='/'+params.id
    //send fetch request
    fetch(url)
        .then((response)=>{
            return response.json();})
        .then((response)=>{
            console.log(response);//,JSON.parse(response));
            if(undefined!==callback)callback(response);
            return response;
        })

}
//httpGet({ressource:'users',id:1});
export function $http(config){
    if(undefined===config.ressource)config.ressource='users';
    let url=ADR_SRV+'/'+config.ressource;
    //complete with id if present
    if(undefined!==config.id)url+='/'+config.id
    let method=(undefined===config.method)?'GET':config.method;

    let xhr=new XMLHttpRequest();
    xhr.open(method,url,true);
    xhr.setRequestHeader('Content-Type','Application/json');
    xhr.onreadystatechange=
    (response)=>{
        if(xhr.readyState<4 || xhr.status>=400 )return;    (undefined!==config.callback)?config.callback(response):console.log(response,response.data);
    }
    
    xhr.send((undefined!==config.data)?JSON.stringify(config.data):'');
} 
