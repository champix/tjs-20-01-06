import React from 'react';
import './Component.Menubar.css'
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter as Router,Link} from 'react-router-dom'

function Menubar(props)
{
    console.log(props);

    let classInverse=(undefined!== props.isBlack && props.isBlack===true)?' navbar-inverse':''; 

    return (
        <nav className={"navbar"+classInverse}>
           <Link to="/" className="navbar-brand">Users-react</Link>
            {/* <a className="navbar-brand" href="#">Title</a> */}
            <ul className="nav navbar-nav">
                <li className="active">
                    <Link to="/users">Users</Link>
                </li>
                <li>
                <Link to="/users/1">Users/1</Link>

                </li>
            </ul>
        </nav>
        

    );
}

export default Menubar