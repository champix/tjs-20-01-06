import React from 'react';
import './Component.User.css';
import httpGet,{$http} from '../../fetch.js'
import {withRouter} from 'react-router-dom';

 class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isEdited:false, user: {}, id: props.match.params.id,props:props};
        //console.log(props,props.match.params.id);

    }

    addUser = () => {
        
        $http(
            {   ressource:'users',
                id:(undefined!==this.state.id)?this.state.id:undefined,
                data:this.state.user,
                callback:(response)=>{
                    console.log(response,response.data,JSON.parse(response.data));
                    this.setState({user:JSON.parse(response.data)});
                },
                method:(undefined!==this.state.id)?'PUT':'POST'
            })
        }
 

    componentDidMount=()=>{
        let config={ ressource: 'users',id:this.props.match.params.id };
        httpGet(config, (response) => {
            console.log(this);
            this.setState({ user: response});
        })
    }

    render() {
        return (

            <div className="window">
                <h1>Utilisateur</h1>
                <img src="../../img/noimg.png" alt="" />
                <br />
                <label htmlFor="login">Login :</label>
                <input onChange={(evt) => {
                    
                    this.setState({isEdited:true, user: Object.assign({}, this.state.user, { name: evt.target.value }) }) }} type="text" name="i_login" placeholder="cliquez pour saisir"
                    id="i_login" value={this.state.user.name} /><br />
                <label htmlFor="name">nom : </label><input onChange={(evt) => { this.setState({isEdited:true, user: Object.assign({}, this.state.user, { name: evt.target.value }) }) }} type="text" name="i_name" placeholder="cliquez pour saisir"
                    id="i_name" value={this.state.user.name} /><br />
                <label htmlFor="pname">prenom : </label><input onChange={(evt) => { this.setState({isEdited:true, user: Object.assign({}, this.state.user, { pname: evt.target.value }) }) }} type="text" name="i_pname" placeholder="cliquez pour saisir"
                    id="i_pname" value={this.state.user.prenom} /><br />
                   {
                    (this.state.isEdited)?
                        <button  onClick={this.addUser}>Ajouter</button>:<></>
                    }
            </div>

        );
    }
}


export default withRouter(User);