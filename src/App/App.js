import React from 'react';
import logo from './logo.svg';
import './App.css';
import Menubar from './Component.Menubar/Component.Menubar'
import User from './Component.User/Component.User';
import Users from './Component.Users/Component.Users';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  RouteProps
} from 'react-router-dom';


function App() {
  return (
    <Router>
      <Menubar path="chemin1" isBlack={true} />
      <Switch>
        <Route path="/users">
          <Users />
          <Route path="/users/:id">
            <User/> 
            {/* id={RouteProps.id} /> */}
          </Route>
        </Route>
        <Route path="*">Page d'accueil</Route>
      </Switch>
    </Router>);
}

export default App;
