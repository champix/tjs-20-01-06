import React from 'react';
import './Component.Users.css'
import httpGet from '../../fetch'

export default class Users extends React.Component{
    constructor(props){
        super(props);
        this.state={users:[{id:0,nom:'Alexandre'}]};

    }
componentDidMount(){
    httpGet({ressource:'users'},(response)=>{
      //  alert('j\'ai bien reçu mes users'+JSON.stringify(response));
      this.setState({users:response});
    })    
}

//    getLastId(){}
    addUser=()=>{
      this.setState({users:[...this.state.users,{nom:this.state.inputValue}]});
    }
    render(){
        return (<div className="users"><h1>Users</h1>
           
           <input onChange={(evt)=>{this.setState({inputValue:evt.target.value})}} type="text" name="i_users-name" id="i_users-name"  />
            <button onClick={this.addUser} >Ajouter</button>
            <hr/>
            <div className="liste-users">
            {
               this.state.users.map((element,index)=>{
                   return (<div key={"user-"+index} className='liste-users-user'>{index}: {element.name+' '+element.prenom}</div>);
               }) 
            }     
            </div>
        </div>);
    }

}
